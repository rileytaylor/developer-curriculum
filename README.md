# FSO Developer Curriculum

> Get Lerned!

https://rileytaylor.gitlab.io/developer-curriculum

This repository contains the static site that powers our Curriculum site, a central repository for information in our tooling and infastructure, as well as helpful information and tips to help our team go fast.

## Contributing to the wealth of knowledge
See [Contributing](docs/contributing.md).

## Development
This project uses [VuePress 1.x](https://v1.vuepress.vuejs.org/), a Vue-powered static site generator, similar to Gatsby or Jekyll.

To run the project locally, do the following:

```shell
npm install
npm run docs:dev
```

Then, check out the site running at [https://localhost:8080/]().
