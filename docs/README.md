---
home: true
heroImage: icons/icon-512.png
actionText: Get Lerned! →
actionLink: /curriculum/
features:
- title: Why?
  details: BECAUSE IT'S GOOD FOR YOU!!!!
- title: Pro Tip
  details: Use the search bar at the top to find what you are looking for.
- title: We heard you like apps
  details: For quick reference and a great welcome to the world of 2019, install this app to your desktop. That's right, it's a PWA. Hit that ... menu on your favorite browser that supports PWA's and install. It's that easy!
footer: MIT Licensed | Copyright © 2019 University of Arizona, Financial Services Office IT
---
