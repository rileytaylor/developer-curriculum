/**
 * TODO: Things to work on:
 * - customize pwa refresh popup?
 * - figure out why the cloudfront isn't working properly for the dev-branch sites
 * - url and ssl cert
 * - algolia docsearch?
 */

module.exports = {
  title: 'Developer Curriculum',
  description: 'FAST Developer resources, tips, tricks, and more',
  dest: 'public',
  base: '/developer-curriculum/',
  evergreen: true,

  // Markdown Configuration
  markdown: {
    extendMarkdown: md => {
      md.use(require('markdown-it-html5-embed'), {
        html5embed: {
          useImageSyntax: true,
          useLinkSyntax: false
        }
      })
      md.use(require('@centerforopenscience/markdown-it-video'), {
        youtube: { width: 640, height: 390 },
        vimeo: { width: 500, height: 281 }
      })
    }
  },

  // Plugins
  plugins: {
    '@vuepress/back-to-top': true,
    '@vuepress/pwa': {
      serviceWorker: true,
      updatePopup: true
    }
  },

  // Set html head assets and metadata
  head: [
    ['link', { rel: 'icon', href: '/icons/icon.svg' }],
    ['link', { rel: 'manifest', href: '/manifest.json' }],
    ['meta', { name: 'theme-color', content: '#4a634e' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
    // ['link', { rel: 'apple-touch-icon', href: `/icons/apple-touch-icon-152x152.png` }],
    ['link', { rel: 'mask-icon', href: '/icons/icon.svg', color: '#4a634e' }],
    ['meta', { name: 'msapplication-TileImage', content: '/icons/icon-144x144.png' }],
    ['meta', { name: 'msapplication-TileColor', content: '#403635' }],
    // Font
    ['link', { rel: 'stylesheet', href: 'https://brand.arizona.edu/sites/default/files/v2/ua-brand-fonts/milo.css'}]
  ],

  // Theme configuration
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Curriculum', link: '/curriculum/' },
      { text: 'Guides', link: '/guides/' },
      { text: 'What I Use', link: '/what-i-use/' },
      { text: 'Contributing', link: '/contributing' }
    ],
    sidebar: {
      '/curriculum/': [
        '/curriculum/',
        {
          title: 'Basics',
          collapsable: false,
          children: [
            'agile',
            'cli',
            'git',
            'macos',
          ]
        },
        {
          title: 'Languages',
          collapsable: false,
          children: [
            'angular',
            'django',
            'html-css',
            'js',
            'python',
            'vuejs',
          ]
        },
        {
          title: 'Tools',
          collapsable: false,
          children: [
            'vagrant',
          ]
        },
        'next-steps',
      ],
      '/guides/': [
        '/guides/',
        {
          title: 'General',
          collapsable: false,
          children: [
            'ssh-keys',
            'api-keys'
          ]
        },
        {
          title: 'Git',
          collapsable: false,
          children: [
            'git-faq',
            'rebasing',
            'resolving-merge-conflicts',
            'associating-your-text-editor-git',
            'git-name-email',
          ]
        },
        {
          title: 'Environment',
          collapsable: false,
          children: [
            'environment',
            'building-vagrant-boxes',
            'vagrant-faq'
          ]
        },
        {
          title: 'Development',
          collapsable: false,
          children: [
            'celery',
            'portal-app-faq',
            'updating-baseapp-portal-app',
            'deploying-portal-applications',
            'mocking-with-responses',
            'importing-exporting-data',
            'integration-testing-nightwatch',
            'linting'
          ]
        }
      ],
      '/what-i-use/': [
        '/what-i-use/',
        'jordan',
        'riley'
      ]
    },
    logo: '/icons/icon.svg',
    repo: 'https://gitlab.fso.arizona.edu/fast/developer-curriculum',
    repoLabel: 'Repository',
    lastUpdated: 'Last Updated',
    docsDir: 'docs',
    docsBranch: 'master',
    docsRepo: 'https://gitlab.fso.arizona.edu/fast/developer-curriculum',
    editLinks: true,
    editLinkText: 'Improve this page!',
    serviceWorker: {
      updatePopup: true
    }
  }
};
