# Riley Taylor

[[toc]]

## Apps and stuff

Text Editor - [Visual Studio Code](https://code.visualstudio.com)

Terminal - [Hyper](https://hyper.is)

Also, use [Homebrew](https://brew.sh/).

Other apps and tools worth checking out:
- [Royal TSX](https://royalapplications.com/ts/mac/features) is a great remote desktop/ssh client. [Microsoft Remote Desktop](https://docs.microsoft.com/en-us/windows-server/remote/remote-desktop-services/clients/remote-desktop-clients) is also good. I keep both around since Microsoft RDP allows use of multiple monitors for when I'm doing a lot of work in windows on my mac while Royal TSX is better at managing a lot of open remote sessions or quick drop-ins on a server.
- [Postman](https://www.getpostman.com/) is invaluable for api testing. When Django Rest Framework's built in api viewer isn't enough (i.e., for testing out json without having to re-enter it every time...) I use postman. Also great for playing around with other people's api's.
- [GitKraken](https://www.gitkraken.com/) is my favorite git GUI client. I rarely use the GUI anymore, but I find this one to be the easiest to use. Sourcetree is okay, but this is more lightweight. You can get a free pro upgrade through Github's [Student Developer Pack](https://education.github.com/pack) with your @email.arizona.edu address.

macOS utilities:
- [Magnet](http://magnet.crowdcafe.com/) is a fantastic macOS window manager. It's a couple bucks, but it's well worth the cost.
- [Cheatsheet](https://www.cheatsheetapp.com/CheatSheet/) is a quick reference to all the keyboard shortcuts of an app
- [Kap](https://electronjs.org/apps/kap) is how I make all those fancy gif's of my computer screen.
- [SpotSpot](https://electronjs.org/apps/spotspot) Spotify is stupidly hard to control on a mac. This makes it a bit easier, plus lets you see what you are listening to on your desktop

#### Package Manager installs:

*Homebrew*
- [nvm](https://github.com/creationix/nvm) - quickly swap out node versions
- [git-radar](https://github.com/michaeldfallen/git-radar) - better git status in your terminal

*npm*
To install these guys, `npm -i -g <name>`
- [autoprefixer](http://autoprefixer.github.io/) - css auto-vendor prefixing
- [eslint](https://eslint.org/) - js linting, nice to have installed globally
- [node-sass](https://github.com/sass/node-sass) - sass compiling with node and c, faster than compass, nice to have installed globally
- [sass-lint](https://github.com/sasstools/sass-lint) - sass linting, nice to have installed globally


## Visual Studio Code
VS Code is where it's at. It has the best native support for python and javascript and the extension ecosystem is wide-ranging. It also does the best at helping with git merge conflicts and debugging.

### Extensions
These are not all of my extensions, but the ones that are extra nice to have.
- [Auto Close Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag)
- [Auto Rename Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag)
- [Autoprefixer](https://marketplace.visualstudio.com/items?itemName=mrmlnc.vscode-autoprefixer) - automatically add browser prefixes to your css
- [Color Info](https://marketplace.visualstudio.com/items?itemName=bierner.color-info)
- [Django Template](https://marketplace.visualstudio.com/items?itemName=bibhasdn.django-html)
- [Document This](https://marketplace.visualstudio.com/items?itemName=joelday.docthis) - Easy JSDoc commenting
- [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig) - Just. Use. It.
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) - Automatically lint your code
- [Git Lens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens) - Much utility such git wow
- [Image preview](https://marketplace.visualstudio.com/items?itemName=kisstkondoros.vscode-gutter-preview)
- [npm](https://marketplace.visualstudio.com/items?itemName=eg2.vscode-npm-script) - see what version is the latest in your `package.json` as well as general npm helpers
- [npm intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.npm-intellisense) - intellisense for your imports
- [Path Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense) - intellisense for paths. Must have.
- [Python](https://marketplace.visualstudio.com/items?itemName=ms-python.python) - Wow does this extension do it all. This makes VS Code a python IDE.
- [Sass](https://marketplace.visualstudio.com/items?itemName=robinbentley.sass-indented)
- [sass lint](https://marketplace.visualstudio.com/items?itemName=glen-84.sass-lint)
- [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur) - all the Vue.js things
- [vscode-icons](https://marketplace.visualstudio.com/items?itemName=robertohuertasm.vscode-icons) - best icon set, IMHO

### Configuration
My user config. Some of these configuration settings are related to extensions.

```json
// Place your settings in this file to overwrite the default settings
{
    // VS Code stuff
    "files.associations": {
        "*.vue": "vue",
        ".prospector.yaml": "yaml"
    },
    "window.zoomLevel": 0,
    // "window.nativeTabs": true,
    "window.restoreWindows": "all",
    "workbench.activityBar.visible": true,
    "workbench.iconTheme": "vscode-icons",
    "workbench.commandPalette.history": 10,
    "editor.detectIndentation": false,
    "editor.cursorBlinking": "solid",
    "editor.minimap.maxColumn": 80,
    "editor.minimap.renderCharacters":false,
    "editor.smoothScrolling": true,
    "vsicons.projectDetection.disableDetect": true,
    "npm.enableScriptExplorer": true,
    "workbench.colorTheme": "One Dark Pro",
    "explorer.confirmDragAndDrop": false,
    "debug.toolBarLocation": "docked",
    "terminal.integrated.drawBoldTextInBrightColors": false,
    "breadcrumbs.enabled": true,

    // Python
    "python.linting.lintOnSave": true,
    "python.linting.pep8Enabled": false,
    "python.linting.pep8Path": "/usr/local/bin/autopep8",
    "python.linting.pylintEnabled": false,
    "python.linting.pylintPath": "/usr/local/bin/pylint",
    "python.linting.pylintArgs": ["--load-plugins", "pylint_django"],
    "python.linting.prospectorEnabled": true,
    "python.linting.prospectorPath": "/usr/local/bin/prospector",
    "python.linting.prospectorArgs": ["--uses", "django", "celery"],
    "python.pythonPath": "/usr/local/bin/python3",

    // ESLint
    "eslint.enable": true,
    "eslint.nodePath": "~/.npm/",
    "eslint.validate": [
        "javascript",
        "html",
        "vue"
    ],

    // HTMLhint
    "htmlhint.options": {
        "doctype-first": false
    },
    
    "vsicons.dontShowNewVersionMessage": true,
    "vsicons.associations.files": [
        { "icon": "rollup",  "extensions": ["rollup.build.js", "rollup.config.js"], "filename": true, "format": "svg" }
    ],
    "vsicons.associations.folders": [
        { "icon": "view", "extensions": ["html", "view", "views", "_view", "_views", "pages"], "format": "svg" }
    ],

    // Gitlens
    "gitlens.codeLens.scopes": [
        "document",
        "containers"
    ],
    "gitlens.recentChanges.highlight.locations": [
        "gutter",
        "line",
        "overview"
    ],
    "gitlens.keymap": "chorded",
    "gitlens.advanced.messages": {
        "suppressCommitHasNoPreviousCommitWarning": false,
        "suppressCommitNotFoundWarning": false,
        "suppressFileNotUnderSourceControlWarning": false,
        "suppressGitVersionWarning": false,
        "suppressLineUncommittedWarning": false,
        "suppressNoRepositoryWarning": false,
        "suppressResultsExplorerNotice": false,
        "suppressShowKeyBindingsNotice": true
    },
    "gitlens.settings.mode": "advanced",
    "gitlens.views.repositories.files.layout": "tree",
    "gitlens.views.fileHistory.enabled": true,
    "gitlens.views.lineHistory.enabled": true,

    // TODO Highlight
    "todohighlight.include": [
        "**/*.js",
        "**/*.jsx",
        "**/*.ts",
        "**/*.tsx",
        "**/*.html",
        "**/*.php",
        "**/*.css",
        "**/*.scss",
        "**/*.md"
    ],
    "[django-html]": {},
}
```

## Hyper

My Hyper configuration (usually `~/.hyper.js`):

```js
module.exports = {
  config: {
    // default font size in pixels for all tabs
    fontSize: 12,

    // font family with optional fallbacks
    fontFamily: 'Menlo, "DejaVu Sans Mono", Consolas, "Lucida Console", monospace',

    // terminal cursor background color and opacity (hex, rgb, hsl, hsv, hwb or cmyk)
    cursorColor: 'rgba(248,28,229,0.8)',

    // `BEAM` for |, `UNDERLINE` for _, `BLOCK` for █
    cursorShape: 'BLOCK',

    // color of the text
    foregroundColor: '#fff',

    // terminal background color
    backgroundColor: '#000',

    // border color (window, tabs)
    borderColor: '#333',

    // custom css to embed in the main window
    css: '',

    // custom css to embed in the terminal window
    termCSS: '',

    // set to `true` (without backticks) if you're using a Linux setup that doesn't show native menus
    // default: `false` on Linux, `true` on Windows (ignored on macOS)
    showHamburgerMenu: '',

    // set to `false` if you want to hide the minimize, maximize and close buttons
    // additionally, set to `'left'` if you want them on the left, like in Ubuntu
    // default: `true` on windows and Linux (ignored on macOS)
    showWindowControls: '',

    // custom padding (css format, i.e.: `top right bottom left`)
    padding: '12px 14px',

    // the full list. if you're going to provide the full color palette,
    // including the 6 x 6 color cubes and the grayscale map, just provide
    // an array here instead of a color map object
    colors: {
      black: '#000000',
      red: '#ff0000',
      green: '#33ff00',
      yellow: '#ffff00',
      blue: '#0066ff',
      magenta: '#cc00ff',
      cyan: '#00ffff',
      white: '#d0d0d0',
      lightBlack: '#808080',
      lightRed: '#ff0000',
      lightGreen: '#33ff00',
      lightYellow: '#ffff00',
      lightBlue: '#0066ff',
      lightMagenta: '#cc00ff',
      lightCyan: '#00ffff',
      lightWhite: '#ffffff'
    },

    // the shell to run when spawning a new session (i.e. /usr/local/bin/fish)
    // if left empty, your system's login shell will be used by default
    // make sure to use a full path if the binary name doesn't work
    // (e.g `C:\\Windows\\System32\\bash.exe` instad of just `bash.exe`)
    // if you're using powershell, make sure to remove the `--login` below
    shell: 'bash',

    // for setting shell arguments (i.e. for using interactive shellArgs: ['-i'])
    // by default ['--login'] will be used
    shellArgs: ['--login'],

    // for environment variables
    env: {},

    // set to false for no bell
    bell: 'SOUND',

    // if true, selected text will automatically be copied to the clipboard
    copyOnSelect: false,

    // if true, on right click selected text will be copied or pasted if no
    // selection is present (true by default on Windows)
    // quickEdit: true

    // URL to custom bell
    // bellSoundURL: 'http://example.com/bell.mp3',

    // for advanced config flags please refer to https://hyper.is/#cfg
    hyperTabs: {
        trafficButtons: true,
        tabIconsColored: true,
    },
    hyperlinks: {
        defaultBrowser: true
    }
  },

  // a list of plugins to fetch and install from npm
  // format: [@org/]project[#version]
  // examples:
  //   `hyperpower`
  //   `@company/project`
  //   `project#1.0.1`
  plugins: [
      //"hyperpower",
      "hyperterm-spacegray",
      "hyper-confirm",
      "hyper-tabs-enhanced",
      "hyper-statusline",
      "hyperterm-paste",
      "hypercwd",
      "hyperlinks",
      "gitrocket"
  ],

  // in development, you can create a directory under
  // `~/.hyper_plugins/local/` and include it here
  // to load it and avoid it being `npm install`ed
  localPlugins: []
};

```

I highly recommend the `hyper-terms-enhanced`, `hypercwd`, and `hyperterm-paste` extensions. See [Awesome Hyper](https://github.com/bnb/awesome-hyper) for other extensions.


## Bash Profile
```shell
# Aliases
alias showfiles='defaults write com.apple.finder AppleShowAllFiles YES; killall Finder /System/Library/CoreServices/Finder.app'
alias hidefiles='defaults write com.apple.finder AppleShowAllFiles NO; killall Finder /System/Library/CoreServices/Finder.app'
alias python='python3'
alias brewup='brew update && brew upgrade'
alias wut='git diff --name-status'

# Setting PATH
PATH="$HOME/.config/yarn/global/node_modules/.bin:/Library/Frameworks/Python.framework/Versions/3.5/bin:/Users/rileytaylor/Library/Developer/Xamarin/android-sdk-macosx/platform-tools:${PATH}"
export PATH

# bashrc
if [ -f ~/.bashrc ]; then
  . ~/.bashrc
fi

# bash completion
if [ -f $(brew --prefix)/etc/bash_completion ]; then
  . $(brew --prefix)/etc/bash_completion
fi

# nvm stuff (it’s installed in homebrew)
export NVM_DIR=~/.nvm
source $(brew --prefix nvm)/nvm.sh

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

# thefuck use `fuck` as alias
eval $(thefuck --alias)
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
```

## .bashrc
```shell
function prompt {
    # colors
    local GREEN="\[\e[1;32m\]"
    local STEEL="\[\e[1;49;94m\]"
    local DEFAULT_COLOR="\[\e[0m\]"

    export PS1="$STEEL[\u]$DEFAULT_COLOR \w \$(git-radar --bash) $GREEN\\n λ $DEFAULT_COLOR"
    export GIT_RADAR_COLOR_BRANCH="\\033[0;36m"
}

function title {
    echo -ne "\033]0;"$*"\007"
}

prompt
title
```
