# Jordan Piel

This is my (minified) `~/.bash_profile`:
```shell
parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

PS1="\[\033[1;32m\]\T | \w\$(parse_git_branch) \n💀   > \[\e[0m\]"

test -f ~/.git-completion.bash && . $_

alias cleardns='sudo killall -HUP mDNSResponder'

alias brewup='brew update && brew upgrade && brew cask upgrade'

alias showfiles='defaults write com.apple.finder AppleShowAllFiles YES; killall Finder /System/Library/CoreServices/Finder.app'
alias hidefiles='defaults write com.apple.finder AppleShowAllFiles NO; killall Finder /System/Library/CoreServices/Finder.app'

alias moon="curl http://wttr.in/Moon"
alias weather="curl http://wttr.in"
```
For git completion download the git-completion bash script
```shell
curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash -o ~/.git-completion.bash
```
