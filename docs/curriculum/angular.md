# AngularJS

::: warning
As of 2019, we don't really worry too much about AngularJS anymore until we need to, so the resources are here for future reference if you are ever put on a project that still uses AngularJS.
:::

Angular is a popular javascript framework that we use extensively. New apps will be using Vue.js, but an understanding of Angular will be important for bug-fixes on older applications.

### Goals:
* Learn JavaScript MVC Framework.
* Modules, Submodules, Controllers, Directives, Routes, etc.
* Learn the best practices and style guide.

### Tutorials:
* [Code School](https://www.codeschool.com/courses/shaping-up-with-angular-js)
* [Code Academy](https://www.codecademy.com/learn/learn-angularjs)
* [PhoneCat Tutorial App](https://docs.angularjs.org/tutorial)
* [Django/Angular](https://thinkster.io/django-angularjs-tutorial)

### Recommended Reading
* [Angular Guide](https://docs.angularjs.org/guide)
* [John Papa's Styleguide](https://github.com/johnpapa/angular-styleguide)
