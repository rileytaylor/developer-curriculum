# MacOS

macOS is our primary development operating system. You'll be living in it daily. Do not forget to attend our 9am prayer to Steve Jobs (simplicity be unto him).

If you are new to macOS, take a look at [this introduction](https://www.apple.com/support/macbasics/).
