# Django

Django serves as the re-usable container for Portal applications as well as the backend for each application. Follow `Writing your First Django App` below, which is Django's own excellent walk-through and highly recommended. Additionally, we use Django Rest Framework to handle our API's, so you might consider building an API with it or following the tutorial linked below.

## Goals
* Learn the MVC (Model View Controller) Framework.
* Go through the django tutorial "Writing your First Django App".
* Go through the Django Rest Framework Tutorial

## Tutorials
* [Writing your First Django App](https://docs.djangoproject.com/en/1.11/intro/)
* [Django Rest Framework Tutorial](http://www.django-rest-framework.org/#tutorial)

## Recommended Reading
* [Django Project](https://www.djangoproject.com/)
* [Django Rest Framework](http://www.django-rest-framework.org/)
* [Tango With Django 3rd Edition](https://leanpub.com/tangowithdjango19/read_sample) - The sample book has a great overview of Django
* [Two Scoops of Django](https://www.twoscoopspress.com/products/two-scoops-of-django-1-11) - Arguably the best "styleguide" for Django. We have a copy of the 1.8 and 1.11 versions.
* [The Django Book](http://djangobook.com/the-django-book/) - great online book, some of it is still version 1.8, but it is still an excellent guide.
