# Javascript

Javascript is what powers our front-end applications. Newer apps are using Vue.js while older apps are using AngularJS. Before diving into frameworks, it is a good idea to understand how javascript works in the first place. jQuery is good to know, but you'll be using it less often.

It is important to note that our Vue.js based apps take advantage of webpack and babel to transpile code down to ES5 (which all browsers support). That means that we can use bleeding-edge javascript! In practice, we use everything released in an official ECMAScript version, as well as features that are stage-3 or stage-4, meaning they are either supported by at least one browser and in a final(ish) state, or are supported by multiple browsers and ready to be added in the next version of the language spec, respectively. With that said, an app may take advantage of a stage-2 proposal, so it's good to familiarize yourself with the entire "living ECMAScript" process.

## Goals
* Understand `AJAX` (Asynchronous JavaScript and XML).
* Understand how to access HTML `DOM` (Document Object Model).
* Understand JavaScript Promises, pre-ES6 and ES6 style.
* Understand ES2015 (ES6), ES2016 (ES7), ES2017, and newer javascript proposals (including stage-3 and stage-4 proposals)
* Use your personal GitHub account to create a `github-viewer` application on [Plunkr](https://plnkr.co/) or [jsFiddle](https://jsfiddle.net/). Use the [GitHub API](https://developer.github.com/v3/) to create an application that allows users to search for a GitHub users. Use plain JavaScript for this project. Or, develop a small utility application of your choice that interacts with an api using ajax.

## Tutorials
* [Code Academy - Javascript](https://www.codecademy.com/learn/javascript)
* [Promises](http://www.sohamkamani.com/blog/2016/08/28/incremenal-tutorial-to-promises/)
* [Deep-Dive on Promises](https://ponyfoo.com/articles/es6-promises-in-depth)

## Required Reading
* [ES6 in 350 bullet points](https://ponyfoo.com/articles/es6)
* [ES2016](https://ponyfoo.com/articles/es2016-features-and-ecmascript-as-a-living-standard)
* [ES2017+](https://ponyfoo.com/articles/tc39-ecmascript-proposals-future-of-javascript)

## Recommended Reading
* [Every Feature added to JS since ES5](https://medium.freecodecamp.org/es5-to-esnext-heres-every-feature-added-to-javascript-since-2015-d0c255e13c6e)
* [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS) - free, open-source, online book series!
* [How to Become a Great Javascript Developer](http://blog.ustunozgur.com/javascript/programming/books/videos/2015/06/17/how_to_be_a_great_javascript_software_developer.html)
* [Exploring ES2016 and ES2017](http://exploringjs.com/es2016-es2017/)
* [Mozilla Developer Network](https://developer.mozilla.org/docs/Web/JavaScript/Reference) - the best javascript reference
* [The TC39 Process](https://tc39.github.io/process-document/)
