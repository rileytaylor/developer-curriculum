# Next Steps

TODO: WIP

With all of your knowledge of git, languages, and development frameworks, it's time to jump into a project. When you do this, it's very important you follow our workflow to a T, which is outlined below. Feel free to refer back to this, however, it'll become second nature after you merge in your first few merge requests.

## 1. Get assigned to a project

This part's easy. Find out what you need to be working on.

## 2. Pick an issue

You will either be told to work on an issue, or be given free reign to pick your own issue. Consult the Gitlab issues for a project, as well as the Redmine issues board.
