# Git

Git is our versioning solution. Everything goes in git. Gitlab is our git provider.

## Goals
* Understand local, staging area, and remote repos.
* Branch, Release, Hotfix, Features, Merge, Rebase, GitLab, etc.
* Setup a personal repo on GitLab with a `.gitignore` and `README.md`.

## Tutorials
* [15-minute git Tutorial](https://try.github.io/levels/1/challenges/1)
* [Code School](https://www.codeschool.com/learn/git)
* [Code Academy](https://www.codecademy.com/learn/learn-git)

## Recommended Reading
* [Oh, Shit, git!](http://ohshitgit.com/) - good things to know for when you inevitably mess something up.

There are several different `flow` models that are in use and debated. Here they are in order of oldest to newest:
* [Git Flow](http://nvie.com/posts/a-successful-git-branching-model/)
* [Github Flow](https://guides.github.com/introduction/flow/)
* [Gitlab Flow](https://about.gitlab.com/2014/09/29/gitlab-flow/)

We use something more akin to Git Flow, but we rebase feature branches and fast-forward merge. We also keep a `develop` and `master` branch, each corresponding to our development servers and production servers, respectively. In the case a project has no development server environment we may skip the `develop` branch.

**Note.** Some older projects will use a Merge commit strategy, but we are phasing that out since it clutters up the git history and adds extra steps since you have to rebase `develop` on `master` after every merge into master.

