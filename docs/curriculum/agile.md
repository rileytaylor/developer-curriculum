# Agile

We use the Agile development methodology. As of 2017 we are still new to the process and improving it all the time. As a part of working on projects here you will learn how *we* do Agile, but below are several links to resources that will help you get going if you do not already know anything about Agile.

* [Agile Cheatsheet](http://www.dummies.com/careers/project-management/agile-project-management-for-dummies-cheat-sheet/)
* [Crash Course](https://yeti.co/resources/videos/intro-to-agile-development-a-crash-course/)
* [Agile Meetings, Demystified](https://www.atlassian.com/agile/ceremonies)
