# Vagrant

Vagrant is the environment we use to containerize our applications, similar to Docker. You will breath, eat, and sleep in Vagrant. Vagrant is all. Amen.

## Goals
* Understand the big picture of how and why we use it.
* Learn about Vagrant providers such as VirtualBox, HyperV, VMware Fusion, etc.)
* Understand separation between local machine and virtual machine.
* Clone [`django-base-app`](https://gitlab.fso.arizona.edu/FAST/django-base-app) and get it running on your machine.

## Tutorials
* [Vagrant Tutorial](https://www.vagrantup.com/docs/getting-started/)
* [Get Vagrant Up and Running](https://scotch.io/tutorials/get-vagrant-up-and-running-in-no-time)

## Recommended Reading
* [Vagrant](https://www.vagrantup.com/)
