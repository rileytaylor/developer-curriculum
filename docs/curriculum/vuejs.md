# Vue.js

Vue.js is a popular javascript framework we are using on new applications going forward. It uses MVVM principles, reactive data stores, and is known for being easy to understand.

## Goals
* Understand the basic principles and organization of Vue.js
* Understand how Vuex works
* Build a simple app

## Learn
1. Watch the [Intro to Vue.js](https://www.vuemastery.com/courses/intro-to-vue-js/vue-instance) course from Vue Mastery
1. Learn about Vuex from [Vuex for Everyone](https://vueschool.io/courses/vuex-for-everyone)
1. Finally, [build a simple app](https://jayway.github.io/vue-js-workshop/)\*

::: tip
Note that this tutorial is a bit old. It should work just fine, but be sure to install `vue-cli@2` when installing the `vue-cli`, otherwise you'll be in for a world of headaches since `vue-cli 3.x` is _very_ different.
:::

## Recommended Reading
* [Vue.js Documentation](https://vuejs.org/guide)
* [Vuex Documentation](https://vuex.vuejs.org/en/intro.html)
* [Vue Mastery Cheatsheet](https://www.vuemastery.com/pdf/Vue-Essentials-Cheat-Sheet.pdf)
* [How not to Vue](https://itnext.io/how-not-to-vue-18f16fe620b5)
* [Why use Vuex?](https://medium.com/vue-mastery/vuex-intro-tutorial-course-38ca0bca7ef4)
