# HTML/CSS

We use HTML and CSS extensively, but you already knew that. We use [Bootstrap](https://getbootstrap.com) as our framework, and make use of css preprocessors like Sass.

## Goals
* Understand HTML5 and CSS3 Markup. You should already know this, but link below will provide some review opportunities.
* Understand Bootstrap 4
* Understand CSS pre-processors like Sass.

## Tutorials
* [Code Academy](https://www.codecademy.com/learn/learn-html)
* [Bootstrap 4 Tutorial](https://www.w3schools.com/bootstrap4/default.asp)\*
* [Learn Sass in 15 minutes](https://tutorialzine.com/2016/01/learn-sass-in-15-minutes)

## Recommended Reading
* [Mozilla Developer Network](https://developer.mozilla.org/en-US/docs/Learn) - this is your go-to reference
* [W3 Schools](http://www.w3schools.com/)
* [Bootstrap](https://fast.pages.fso.arizona.edu/fso-bootstrap)
* [Sass](http://sass-lang.com/guide) - We generally use the SCSS syntax
