# Command Line, Terminal Emulators, and Text Editors

You are welcome to use zsh, fish, or whatever you want, but most of our processes are based on bash. Much of your work will be done in a terminal window. You are welcome to use whatever editor you wish, though an understanding of Vi/Vim is important since that will be the default editor in your terminal.
* Note that Vi is built into macOS and Linux, only install Vim if you want to.

## Command Line

### Goals
* Learn the basics of Command Line.
* Learn the basics of Vim. Unless Vim is your text editor of choice, chances are you will most likely run into Vim during development.
* Get set up on a text editor
* Create your own `.bash_profile` with your own custom settings, paths, and aliases, etc.

### Tutorials
* Command Line
    * [Code Academy](https://www.codecademy.com/learn/learn-the-command-line)
    * [Learn Enough Command Line to Be Dangerous](https://www.learnenough.com/command-line-tutorial#cha-command_line)
    * [Bash Academy](http://www.bash.academy/)
* Vi/Vim
    * [Beginners Guide to Vim](https://www.linux.com/learn/vim-101-beginners-guide-vim) - This presents all you really need to know to get by with occasional vi/vim use.
    * [Vim Cheatsheet](http://vim.rtorr.com/)

### Recommended Reading
* [Vim](http://www.vim.org/)
* [Open Vim Tutorial](http://www.openvim.com/)
* [Vim Adventures](http://vim-adventures.com/)

## Terminal Emulators
Terminal emulators can make your life much easier. While the built-in terminals of macOS and most Linux distro's are usually pretty good, you can do better.

Recommended terminal emulators include:
- [iTerm 2](https://iterm2.com) is the macOS terminal turned up to 11
- [Hyper](https://hyper.is) is a html/css/js based electron app that is cross-platform and installs extensions through npm.

## Text Editors
You are welcome to use any text editor to do your work, however, we recommend using something that allows you to configure `eslint`, `pylint`, and allows use of `.editorconfig` files so that we can keep our code in sync stylistically.

Recommended text editors include:
- [Visual Studio Code](https://code.visualstudio.com)
- [Sublime](http://www.sublimetext.com/) - if you don't already own it yourself, we have a license for the office, just ask!
- [Atom](https://atom.io/)

Required extension:
- [Editor Config](http://editorconfig.org/#download)
