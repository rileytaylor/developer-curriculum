# Python

Python powers our application backends. You'll use it with Django.

## Goals
* Learn the basic data structures, conventions, and code styles of the language.
* Understand `virtualenv` and `virtualenvwrapper` to create isolated python environments. Read about them for now, you will use them in the Django tutorials.

## Tutorials
* [Learn Python](http://www.learnpython.org/)
* [Code School](https://www.codeschool.com/learn/python)
* [Code Academy](https://www.codecademy.com/learn/python)

## Recommended Reading
* [Python.org](https://www.python.org/)
* [Python Guide](http://docs.python-guide.org/en/latest/)
* [Virtualenv (python guide)](http://docs.python-guide.org/en/latest/dev/virtualenvs/)
* [VirtualEnv PPA](https://virtualenv.pypa.io/en/stable/)
* [VirtualEnvWrapper](https://virtualenvwrapper.readthedocs.io/en/latest/) 
