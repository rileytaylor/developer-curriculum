# Curriculum

![Funny Gif](https://media.giphy.com/media/E5dJi6ZRvlZ8Q/giphy.gif)

The topics presented here list all languages, philosophies, and technologies we usually work with. In addition to everything presented below we also have many API's built with C# and/or PowerShell.

Most topic pages include a `Tutorials` section and a `Recommended Reading` section. Do not feel required to do every tutorial present, just do the ones that appeal to you. As often as possible we present at least one quick tutorial and one long-form learning.

To get started, work through the Basics section of the outline. You may already know many of these things, but ensuring you can move around the command line and use the very basics of Git is important to get going. You can always flesh out your understanding of Git and the Command Line through learning the tooling around our development platforms.

From there, choose either Python and Django, or Javascript and Vue.js to start with. It is recommended you start with the one you are least familiar with, but it is also worth checking with the team to see which you will likely be working with the most at first. Either way, you should go through both paths.

Remember to always ask other developers questions, find resources of your own, and try things out!

![sonic school](/uploads/sonic-school.jpg)
