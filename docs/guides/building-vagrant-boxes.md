# Building Vagrant Boxes

There are many ways to build vagrant boxes, however, the preferred method here at FSO is to use [Bento](http://chef.github.io/bento/), a tool created by the awesome developers at [Chef](). This guide also includes a step-by-step process for building Vagrant boxes from scratch as an example, but it should never be used to build any of our standard images or something we can build with Bento.
<hr>

::: tip For the Curious...
there is an older project, [Ubuntu-x64](https://gitlab.fso.arizona.edu/FAST/ubuntu-x64), which also automated this process but has been deprecated in favor of Bento.
:::

## Using Bento

Bento is a set of configurations that are used by [Packer](https://www.packer.io/), a tool created by the same people who make Vagrant, to build boxes in an automated manner. The idea of Bento is to build boxes by installing and configuring the image before it is fully provisioned, which avoids contaminating the image. While it will rarely cause any problems, entering a box after it has been provisioned by Vagrant and modifying it contaminates the image since we cannot guarantee that the box will be exactly the same between versions, nor that a rebuild of the box would perform the same. Bento solves this problem by handling the provisioning process for us without requiring any manual script running, which eliminates human error. Additionally, Bento is just easy to use.

::: danger Danger Zone!
As a general rule, if you do anything to the box after running packer with bento's configuration (i.e. `vagrant ssh`'ing into the image and running commands manually) before uploading it to Vagrant Cloud, you are doing it wrong.
:::

### Getting Started

1. First, you will need Packer installed. If you are on a mac, use homebrew!
```bash
brew install packer
```

1. To use bento, we need to clone the repository locally:
```bash
git clone git@github.com:chef/bento.git
cd bento
```

We use Ubuntu for just about everything here, so we will build an Ubuntu image. Many other operating systems are available too, such as CentOS and Fedora.

### One simple change

1. We make one simple change to make things slightly easier and to ensure the image loads inside vagrant with all of its packages and kernel fully up to date. To make the change, navigate to `ubuntu/scripts`, then open `update.sh` with your favorite text editor. In this example we'll use vim since you'll have it pre-installed on your mac:
```bash
cd `ubuntu/scripts`
vim update.sh
```

1. Then, add `reboot now` to the end of the file after `apt-get -y dist-upgrade -o Dpkg::Options::="--force-confnew";` so that it looks like:
```bash
# Upgrade all installed packages incl. kernel and kernel headers
apt-get -y dist-upgrade -o Dpkg::Options::="--force-confnew";
reboot now
```

Save the file, `cd` back to the ubuntu directory (`cd ../..` should do the trick!), then you are ready to build an image.

::: warning
You'll want to `git fetch` and `git pull` changes from the github repo *before* building any boxes every time you build a new box. You might need to resolve merge conflicts for this change, but that should be rare due to its placement in the file.
:::

### Building an Ubuntu Image

To build an ubuntu box (we will build an Ubuntu 16.04 LTS box for this example), type the following:
```bash
cd ubuntu
packer build -only=virtualbox-iso ubuntu-16.04-amd64.json
```

What this does, in a nutshell, is pull the latest Ubuntu 16.04 LTS image for x64-based systems and then update to the latest supported kernel, update to the latest packages via `apt`, and prepare the image for use within a vagrant box, which includes pruning packages and operating system components that arent necessary to create the smallest image possible. As of early 2018, this tends to produce an Ubuntu 16.04 image that is ~400mb and the kernel will be on the `4.4.0` track, which is SLTS (Super Long Term Support).

### Verifying the image

1. Once that finishes, you'll have an image sitting in the `builds` folder of the repository. Now we should change to it and test it!
```bash
cd ../builds
# Add the box to vagrant
vagrant box add --name <test-name-whatever-you-want> ubuntu-16.04.virtualbox.box
```
The `.box` file could be different; the cli will tell you what it is called when it is built.

1. Now, get out of the repository to somewhere else in your filesystem and let's test the vagrant image. For this example, we will do this in `~/documents`:
```bash
cd ~/documents
mkdir test-ubuntu-16-box
touch Vagrantfile
vim Vagrantfile
```

1. Paste the following into the Vagrantfile (using your favorite editor), replacing `<box>` with whatever you named the box earlier when running `vagrant box add`:
```ruby
Vagrant.configure("2") do |config|
    config.vm.box = "<box>"
end
```

1. Now, exit your editor and provision!
```bash
vagrant up
```

You should see Vagrant go through the building process. As long as it builds, things should be good to go! For good measure, `ssh` into the box and verify that the operating system, kernel version, and package updates are as they should be. Generally, the kernel should be whatever is mostly recently listed for support by the distro vendor and there should be 0 package updates available. Something like:

![image](/uploads/vagrant.png)

#### Doing it again

After the first time you've done this, you should be able to skip the initial steps of setting up bento and packer, and start by updating your local clone of `chef/bento`, ensuring the `restart now` is in the `ubuntu/scripts/update.sh` file, and then running packer.

### Sharing the box

If you already have an account, log in to [Vagrant Cloud](https://app.vagrantup.com/boxes/search) to update/upload the latest version of your Vagrant box, if you don't have an account you'll have to create one. Be sure to give your box a version and short description of what the box is. If you are building the team box, make sure you log into the fso account.

#### Versioning the fso box

Generally, we name the boxes by their ubuntu codename and architecture (i.e. Ubuntu 16.04 will be `fso/xenial64`). 

Versioning is simple: `yyyy.mm.dd.x`, with `x` being the version of that generated box. Since we build boxes whenever there are updates to the linux servers (usually monthly) this should always start at 0, and increment if any issues are discovered after use. Even if the date for the generated box is different, increment the build for the monthly box rather than create a new version. This is also since the kernel shouldn't change drastically from the servers.

So, if `2018.01.10.0` is released, then an issue is discovered that the canonical team fixes and it effects our development, a new box should be created that includes that fix (using the same process) and then uploaded as `2018.01.10.1`, even if the actual date the patched box is provisioned is several days later.

#### Updating the fso box

The fso box should be updated whenever the servers are updated during monthly maintenance. This should be done the morning afterwards and the generated boxes should use the same kernel as the servers.

## Creating a Vagrant box (the long way)

::: warning Disclaimer
Generally, you should use bento or packer (see above). This is the documented process from before we discovered those tools, and may be out of date.
:::

To create a virtual machine, you need a virtualization provider. Although Vagrant doesn’t discriminate over which provider to use, we’ll be using VirtualBox in the example.

You need to install [VirtualBox](https://www.virtualbox.org/) first before creating the virtual machine. Next, head over to the [Ubuntu download page](https://www.ubuntu.com/download/server) and grab the Ubuntu LTS ISO.

### Installing Ubuntu

After the ISO file finishes downloading, fire up VirtualBox and click on New to create a new virtual machine then select Linux as the type, Ubuntu (64 bit) as the version and give your virtual machine a name. 

On the next pane, select Create a virtual hard drive now then select VMDK (Virtual Machine Disk).

On the Storage on physical hard drive pane select Dynamically allocated and set a minimum of 8 GB. Click continue and your virtual machine is almost finished.

You need to insert the downloaded Ubuntu ISO file as a virtual CD, so upon starting the virtual machine the operating system gets installed. It’s just like how you insert a bootable CD into a CD-ROM.

Now we are ready to fire up the virtual machine and install Ubuntu as the guest OS. There are a couple more steps in the installation process such as selecting the language, country, etc. the more important ones are below.

* When prompted to enter the hostname, type _vagrant_
* For both the username and password also type _vagrant_
* Encrypt your home directory? Select No
* On the timezone panel, select UTC or your own preferred timezone
* Partitioning method: _Guided – use entire disk and set up LVM_
* When prompted which software to install, select OpenSSH server.
* Select to install GRUB boot loader on the master boot record

This should sum up the installation, next you’ll be prompted to login. Use the vagrant user and password to do so and then switch to the root user by typing:

```shell
sudo su -
```

Using the root user, update to the latest packages by typing the following commands:

```shell
apt update
apt upgrade
```

### Configuring the virtual machine for Vagrant

All of the operations carried out by Vagrant will use the vagrant user and all of the sudo commands need to execute without asking for a password each time they are ran. To set this up we need to run the `visudo` command to configure password-less sudo for the user. Just type:

```shell
visudo
```

Add the following line to the end of the file and save it:

```shell
vagrant ALL=(ALL) NOPASSWD:ALL
```

A general guideline when creating Vagrant boxes is to set the root password to a well-known one. Vagrant uses _vagrant_ as the password. To set this up type in the following command as the root user and type in the password when prompted:

```shell
passwd
```

In order for Vagrant to be able to SSH into the virtual machine, it uses public key authentication. We need to set this up for our vagrant user. Still logged in with the root user go to the vagrant user’s home directory:

```shell
cd /home/vagrant
mkdir .ssh
```

Vagrant uses an insecure keypair to connect to the virtual machine, which you can download [here](github.com/mitchellh/vagrant/blob/master/keys/vagrant.pub) Just run the following command to get it directly:

```shell
wget https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub -O .ssh/authorized_keys
```

OpenSSH is very strict about this folder and file permission, so let’s change it to the correct one:

```shell
chmod 700 .ssh
chmod 600 .ssh/authorized_keys
chown -R vagrant: .ssh
```

_Optional step_: To speed up SSH connections to the virtual machine we need to modify the SSH server configuration also:

```shell
vim /etc/ssh/sshd_config
```

Add this line to the end of the file and save it:

```shell
UseDNS no
```

Then restart the SSH server and log out as root:

```shell
service ssh restart
```

### Installing VirtualBox guest additions

In order for Vagrant to correctly share folders between the guest and host operating systems, the VirtualBox Guest Additions needs to be installed. A prerequisite to installing guest additions on Linux systems are a couple of packages such as the Linux headers and developer tools. Let’s install those first, by typing the following:

```shell
sudo apt-get install linux-headers-generic build-essential dkms -y
```

Next, we need to make sure the guest additions image is inserted into the virtual machine’s virtual CD-ROM. To do this focus on the virtual machine’s window, select Devices -> Insert Guest Additions CD Image. Next, we need to mount the CD and run the actual installation shell script, by running the following commands:

```shell
sudo mount /dev/cdrom /media/cdrom
cd /media/cdrom
sudo ./VBoxLinuxAdditions.run
```

After this operation, it’s good to restart the virtual machine to make sure the new settings take effect:

```shell
sudo reboot now
```

### Make the box as small as possible

A couple of commands will reduce the size of the Vagrant box substantially.

This will clean up any unused and old packages:

```shell
sudo apt autoclean && sudo apt autoremove --purge
```

Next, let's "zero out" the drive:

```shell
sudo dd if=/dev/zero of=/EMPTY bs=1M
sudo rm -f /EMPTY
```

Lastly, remove the bash history and logout.

```shell
cat /dev/null > ~/.bash_history && history -c && exit
```

### Creating the base box package

Now, that we created the basic virtual machine, we need to create the actual Vagrant base box. Just type the following command in your actual host machine’s terminal (not in your virtual machine’s command line):

```shell
vagrant package --base <virtualmachine name>
```

`<virtual machine name>` is the virtual machine’s name you specified in the VirtualBox settings. If you don’t know what the name is you can type the following command to find out

```
VBoxManage list vms
```

This process will result in a `package.box` file which is an archive of the virtual machine and Vagrant’s metadata files.
