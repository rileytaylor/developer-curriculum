# Updating Baseapp in a Portal Application

Whenever [`django-base-app`](https://gitlab.fso.arizona.edu/FAST/django-base-app) is updated to a new version, it is a good idea to update the current portal application you are working on. Apps that aren't in active development will usually get updated when a bugfix or feature is next added to them, or if there is a major security flaw that needs to be fixed.

Updating from `django-base-app` is somewhat complex, since you will be merging changes from your project with those from another project, much like updating a fork. In some cases, an app may require specific changes that will need to be resolved through merge conflicts, though as `django-base-app` evolves these necessary changes will happen less often. In most cases, you'll need to let git know that you want to keep the apps name rather than the initial default `my-app` that is used as a placeholder.

## The strategy

There are two ways to go about this: a rebase, or a merge. 

The recommended way to do this is through a merge since there are many changes that will be from varying times, and rebasing the entirety of your application so that it is placed after all of `django-base-app` can lead to a very, very complicated rebase resolution process. By merging, while the history of `django-base-app` and your portal application will be more convoluted, you'll be able to get on with your life. Merge as a strategy does have its downsides, however, since we do make changes to the files that are shared via `django-base-app`, we will be better off. 

Now, if an application is relatively new in development, or is an app created using the older `django-base-app` 1.x era of code, [you may want to instead perform a rebase](#so-im-updating-a-1x-era-app-halp). You'll still need to resolve conflicts, however doing this over 10-15 commits isn't so bad, and it makes your app changes stay together.

## Alright, lets do this!

![](https://media.giphy.com/media/RrVzUOXldFe8M/giphy.gif)

The first thing you need to do is set up `django-base-app` as a remote:

```shell
$ git remote add baseapp git@gitlab.fso.arizona.edu:FAST/django-base-app.git
```

Now, you should have two remotes. `origin`, which is the project's git repo, and `baseapp`, which refers to `django-base-app`

```shell
$ git remote
baseapp
origin
```

Make sure you update your local refs for both remotes:

```shell
$ git fetch --all
```

Create a feature branch. Don't forget to use the issue number (replace the `#` below). Make an issue if you haven't yet. Then merge!

```shell
$ git branch #-base-app
$ git checkout #-base-app
$ git merge baseapp/master
```

Git will attempt to auto-merge and let you know what changes there are to make. It will probably look something like this:

```text
Auto-merging setup/scripts/production.bash
Auto-merging setup/scripts/local.bash
CONFLICT (modify/delete): myapp/package.json deleted in HEAD and modified in baseapp/master. Version baseapp/master of myapp/package.json left in tree.
Auto-merging directory/requirements/local.txt
Auto-merging directory/requirements/base.txt
Auto-merging directory/manage.py
Auto-merging directory/directory/templates/includes/_navbar.html
Auto-merging directory/directory/templates/includes/_footer.html
Auto-merging directory/directory/static/css/django-base-app.css
Auto-merging directory/.pylintrc
Auto-merging README.md
Auto-merging CHANGELOG.md
Auto-merging .gitlab-ci.yml
CONFLICT (content): Merge conflict in .gitlab-ci.yml
Automatic merge failed; fix conflicts and then commit the result.
```

Open up your text editor and fix the conflicts. Then make a commit, usually titled along the lines of `fixing merge conflicts with baseapp vX.X.X`, with the version of `django-base-app` noted. *Make sure you get ALL merge conflicts.* Merging in conflict resolution text is not fun and will break the app, leading to further embarrassing bug fixes. Also, a kitten dies.

### What do I select?

You'll generally run into 2 situations:
1. General dotfiles, `setup/`, etc are usually best to keep the same as base app, so accept the incoming change. **Except** if the change includes app-specific names (i.e. you'll want to take the base app change and rename `my-app` to `renewit`.
2. **The `myapp/` folder** Changes that live inside the `myapp` folder on base-app will go here. You'll want to look over these files and write any changes in them *over* the changes made in your project folder (i.e. `renewit`, `pcoc`, or whatever). This is where the bulk of your work will be. Once you've migrated the changes out of `myapp/` into their homes inside your project's folder, you can delete this folder.

To be sure you've caught all `myapp` references, run the script at: `setup/utils/startapp`, which will change all necessary `myapp` references.

Now that you've taken care of everything:

```shell
$ git add .
$ git commit -m 'fixing merge conflicts with baseapp vX.X.X'
$ git push
```

Now you'll want to test that the app runs. If you had to make any merge conflict resolutions to `setup/` or the `Vagrantfile`, be sure to destroy your vagrant box and reprovision it to make sure everything is good to go.

Then, create a merge request like any other feature branch.

### So, i'm updating a 1.x era app. halp

![](https://media.giphy.com/media/JWiPeGrr8Ms92/giphy.gif)

If your project's app structure looks like:

```text
<root>
|- project
|   |- docs
|   |- <app-name>
|   |- requirements
|   ...dotfiles
|- setup
...dotfiles
```

You have more work to do. Sorry.


