# SSH Keys

Configuring SSH keys is necessary to work in our environment. You will primarily use an ssh key to authenticate with Gitlab, but you will also need one to remote into our development and production servers.

## Creating a Key

First, make sure you don't already have one.

```shell
cat ~/.ssh/id_ed25519.pub
```

If you get a printout of a public key, you already have one. Otherwise, let's make one.

*Note:* We recommend using `ed25519` for a lot of reasons. [See this](https://ed25519.cr.yp.to/) to find out why.

```shell
ssh-keygen -t ed25519 -C "<your-email@email.arizona.edu>"
```

Next, it will prompt you for a location to store the key. This defaults to `~/.ssh/`, which is a good place regardless. Just hit enter unless you want to store it elsewhere.

Finally, you'll be asked to create a password. This is the password you will use to verify your key whenever it is requested for authentication.

## Using a key

Now that you have a key generated, you'll need to copy the public key to use in gitlab or to have it stored on our servers so that you can authenticate with them.

```shell
pbcopy < ~/.ssh/id_ed25519.pub
```

This command will copy the public key to your clipboard.

To add the key to gitlab, go [here](https://gitlab.fso.arizona.edu/profile/keys) and paste the key in. Give it a descriptive title, like `work` or `mac`, so that if you ever have multiple keys for multiple computers, you can easily tell which is which.
