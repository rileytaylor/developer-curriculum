## How do I undo a rebase?
It happens, you broke something on a rebase or you realize you resolved things incorrectly. It's quite easy to get yourself back to before the rebase.

First, use `git reflog` to see the history of _actions_ you've taken. Find the commit before the rebase.

Now, `git reset --hard HEAD@{X}`, with X being the commit listed just before the rebase. You'll now have a clean slate!

## I made one change after committing, but don't want to make a new commit and rebase/squash. What do I do?
Rebasing and Squashing isn't super difficult, but you can simplify your life if you just want to add a few changes to your previous commit. `git add` your changes as normal, then use `git commit --amend` to add them to your previous commit. Your editor will pop up, allowing you to change the commit message. Now you'll have a new commit with the changes of your previous commit _and_ the new things you just added. Because this is a new commit that replaces your old one, you'll then need to `git push --force` if that replaced commit is on Gitlab.

## Changing the commit author
You made a git commit in the vagrant shell, didn't you? Bad! When this happens, your commit is given a strange author name and email, which gitlab won't accept. To change this (OUTSIDE OF VAGRANT LIKE A SENSIBLE PERSON) you can simply use `git commit --amend --author="John Doe <john@doe.org>"`, using the name and email gitlab knows you as. Now you should be able to commit just fine.