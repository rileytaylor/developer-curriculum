# Importing/Exporting App Data from Others

Sometimes when you are testing or debugging a Django app it can be helpful to have a bunch of data in the application to work with. Now you could just spend some time creating fake data but wouldn't you rather be lazy and steal data from a dev or production server?

Of course you would.

## How Do Though?
It is actually quite simple all it requires is making use of two Django management commands [`dumpdata`](https://docs.djangoproject.com/en/1.11/ref/django-admin/#dumpdata) and [`loaddata`](https://docs.djangoproject.com/en/1.11/ref/django-admin/#loaddata).

### Step By Step
![](https://media.giphy.com/media/69jHgu2mpyYUBDxIU6/giphy.gif)

1.  SSH into the box that has the data you want to steal, I mean appropriate.
2.  Navigate to the correct directory, activate the virtualenv, source the environment variables, and navigate down one level so you can use the `manage.py` file.
```
cd /var/www/<APPNAME>
workon <APPNAME>
. /etc/apache2/envvars
cd <APPNAME>
```
3.  Once there run the very specific `dumpdata` command found below and make sure to pipe its output into a .json file as shown, in this case db.json.
```
python manage.py dumpdata --natural-foreign --exclude auth.permission --exclude contenttypes --exclude gatekeeper --indent 4 > db.json
```
4.  Use SCP to beat a hasty retreat with your "liberated" data. In this instance it is being saved to a tmp directory at the root of the host system. Run this command on your own system obviously.
```
scp <USERNAME>@<APPNAME>.fso.arizona.edu:<PATH_TO_JSON_FILE> /tmp
```
::: warning WAIT!
![](https://media1.giphy.com/media/R72pVvq3eDo5y/giphy.gif?cid=3640f6095c67380f304b64626720f871)

Before you try use `loaddata` you need to manually clean the file. This is because even though we told Django not to dump gatekeeper data there are some references that will bjork the import.

Pop open the json file in your favorite text editor and search for "gatekeeper". Wherever you see that word delete the entire object that it is inside. In the example below you would delete the entire visible object.
:::

![](/uploads/import-export-json.png)

1.  With that done navigate to the directory that holds the `management.py` file for the app you want to shove this data into. Then run the command below. Remember that you will want to make sure the file you are trying to load exists in vagrant.
```
python manage.py loaddata -i <PATH_TO_JSON_FILE>
```

Just like that you should have a boatload of data in the app, enjoy.

![](https://media2.giphy.com/media/jX3XmgJ0wocdG/giphy.gif?cid=3640f6095c673a94354f375673656823) 
