# Linting

At FSO we tend to stick to framework-recommended style guides. One notable exception is using 4 spaces rather than 2 in javascript. If you have questions about a particular framework and how to format things, see below. In most cases you should be able to set up your text editor to automatically lint these things. All projects based on `django-base-app` have `.editorconfig`, `.pylintrc`, and `.eslintrc.js` files to keep everyone on the same page. Please, for the love of Linus Torvalds, install the requisite linting extensions to make use of these files. Your merge request will be denied and a kitten will die every time you don't use linting.

## .editorConfig?
[Editor Config](https://editorconfig.org/) is an outstanding tool that lets us sync IDE/Editor settings between different computers and editors. Most of our projects include a configuration that is intended for that project's requirements, and all you need to do is install the required plugin for your editor of choice.

## Vue.js
Our Vue apps are linted with [eslint](https://eslint.org).

## Python
Our Python apps are linted with [prospector](https://prospector.readthedocs.io/en/master/), which combines the power of many python linters (i.e. pylint, pep8, pydocstyle) into one tool.
