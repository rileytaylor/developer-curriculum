# Mocking API Repsonses

**Responses** is a mocking library that works with `requests`. Using a method decorator for a function definition, you can add URLs to a backend list. Responses supports REST api format, and the return type will be a `response` object. You can mock GET, PUT, POST requests as well as specify the return type (ConnectionError, status codes.. 200, 404, etc, json objects). Once a URL has been added to Responses, any local or nested function that calls *requests* will return mock data.

Some examples:
```python
@responses.activate
def test_api(self):
    responses.add(responses.GET, 'https://adapi.fso.arizona.edu',
                  json={'name' : 'joven'}, status=200)
    responses.add(responses.GET, 'https://adapi.fso.arizona.edu/users/get',
                  json=testdata_1, status=404)
    responses.add(responses.GET, 'https://adapi.fso.arizona.edu/users',
                  body=Exception('...'))
    responses.add(responses.POST, 'https://hooks.slack.com/services', status=200)

    resp = requests.get('https://adapi.fso.arizona.edu')
    assertTrue(resp.status_code == 200)
    assertTrue(resp.json()['name'] == 'joven')

```

If the *same* URL is added multiple times to a request type, the mock data will be fetched in sequential access (starting with the first URL defined). Anything more advanced will require employing *Dynamic Responses* (adding a callback function for the return data). More info can be found at https://github.com/getsentry/responses

*Responses* can be installed with `pip install responses`, and used with a simple `import responses`. 
