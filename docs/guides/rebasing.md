## What is a rebase?

With large codebases, there are many different people working simultaneously. After submitting a merge request, chances are that someone else's merge request will be merged into `develop` before your merge request, resulting in your feature branch being behind. When this happens, your branch goes "stale" and needs to be revitalized through a rebase. While stale, conflicts may occur if someone also edited a line you edited.

Understanding how a rebase works requires some knowledge of how Git works.

![master](/uploads/master.png)

To understand this, we need to understand a bit about how Git works. A Git repository is a tree structure, where the nodes of the tree are commits. Here's an example of a very simple repository: it has four commits on the master branch, and each commit has an ID (in this case, `a`, `b`, `c`, and `d`). You'll notice that `d` is currently the latest commit (or HEAD) of the `master` branch.

![branch](/uploads/branch.png)

Here, we have two branches: `master` and `my-branch`. You can see that `master` and `my-branch` both contain commits `a` and `b`, but then they start to diverge: `master` contains `c` and `d`, while `my-branch` contains `e` and `f`. `b` is said to be the "merge base" of `my-branch` in comparison to `master` -- or more commonly, just the "base". It makes sense: you can see that `my-branch` was based on a previous version of `master`.

So let's say that `my-branch` has gone stale, and you want to bring it up to date with the latest version of `master`. To put it another way, `my-branch` needs to contain `c` and `d`. You could do a merge, but that causes the branch to contain weird merge commits that make reviewing the pull request much more difficult. Instead, you can do a [rebase](http://www.git-scm.com/book/en/Git-Branching-Rebasing).

![rebase](/uploads/rebase.png)

When you rebase, Git finds the base of your branch (in this case, `b`), finds all the commits between that base and HEAD (in this case, `e` and `f`), and *re-plays* those commits on the HEAD of the branch you're rebasing onto (in this case, `master`). Git actually creates *new commits* that represent what your changes look like *on top of* `master`: in the diagram, these commits are called `e′` and `f′`. Git doesn't erase your previous commits: `e` and `f` are left untouched, and if something goes wrong with the rebase, you can go right back to the way things used to be.

Another thing to notice, however, is that Git treats branches as merely labels. The `master` branch is whatever commit the `master` label is pointing to, as well as all of that commit's parents. When you rebase a branch, Git moves the branch label to point at the newly-created commits: `my-branch` is no longer pointing at `f`, it's now pointing at `f′`. Going back to the way things used to be just consists of changing that branch label so that it points back at `f`.

You'll notice that there is not a direct path from `f` to `f′`: from the point of view of anyone else watching `my-branch`, history has suddenly changed. In effect, `c` and `d` have been injected into `my-branch`'s history, as if they had been there the entire time. Unlike most version control systems, Git allows you to change the history of your project -- but it is very cautious about letting you do so.

*The above section was adapted from [EdX's rebase guide](https://github.com/edx/edx-platform/wiki/How-to-Rebase-a-Pull-Request)*

Before starting, you might want to [Associate your text editor with Git](helpful-information/associating-your-text-editor-with-git) to make your life easier.

## Squashing?

Squashing is important to reduce the number of commits that go into `develop` and keep the history simpler. 

Often a feature branch may include a number of large commits as well as a few smaller ones for minor edits (like spelling and formatting fixes). This is good: you should always commit often on your feature branch. However, once everything has been hashed out and your pull request has been approved, we do not necessarily need the history of every commit and its respective message. For this reason, it is good practice to "squash" our commits into one before merging a request. 

The process of "squashing" your commits is done through a rebase.

#### What about Gitlab's automatic squashing feature?
Gitlab provides a tool to allow squashing automatically when a merge request is approved. While it is a lot simpler to use, it takes away all of your control over the process. It also has an annoying side effect: It will append all of your commit messages together into a giant commit message, which is incredibly unhelpful for anyone looking at your commit in the future. Just do it through the command line.

### How to Squash

To squash the commits in your branch, you will need to find out how many commit's you've made since diverging from develop. An easy way to see this is by looking at gitlab, assuming you've pushed all your commits up to your branch. If not, git can help us out:

```shell
git log --oneline
```

`--oneline` will simpllify the git log so that there is less scrolling involved, though you can always check out [git-log formatting documentation](https://www.git-scm.com/docs/git-log#_commit_formatting) to see other ways to do this that you may prefer.

Note that this will print your entire git history... which can be upwards of 300 commits for our projects based on `django-base-app`. To show more lines hit `enter`. To get out, simply type `q`. (Git more or less opens up a light version of `vi`)

You may want to limit the commits shown. If you have been paying attention, you may remember that you have only made... 3 commits? Or at least you think you have. In that case, you can use the `-X` option, with the X being the number of commits you want to see. `git log -4` will only show the 4 most recent commits.

Now that you have the number of commits you've made, we can actually initiate the rebase.

```shell
git rebase -i HEAD~X
```

**Note:** The `-i` flag specifies that you want to use interactive mode. Always use interactive mode since it allows you to edit the commit history and is the only way to complete all of the steps for squashing. Additionally, it gives you a chance to confirm that you have selected the right number of commits to rebase before actually going through the process.

With `X` being the number of commits you have made. `HEAD` represents the current state of your branch. In the above shell command, we are telling git to `rebase the last X number of commits against the current state of the branch, in interactive mode`.

From here, you'll see something like the following in your text editor:

```text
pick f48d47c The first commit I did
pick fd4e046 The second commit I did
pick f45dd82 The third commit I did
```

You'll want to change everything after your first commit (the top one) from `pick` to `squash`. This tells git you want to squash these commits into the first one. Save and close the file. If you don't care about the commit message of a particular commit, use `fixup`, which will squash it and automatically comment out the commit message in the next step.

From here, you'll get an editor that will let you change the commit messages.

```text
# This is a combination of 3 commits.
# The first commit's message is:
The first commit I did

# This is the 2nd commit message:

The second commit I did

# This is the 3rd commit message:

The third commit I did
```

You'll want to comment out all messages except the ones you want to keep. Deleting text works just fine too. Remember to keep your commit messages as short as possible while still explaining what was changed. Try to avoid going above 100 characters whenever possible.

Once you save the file and close the editor, git will work it's magic. You may run into a merge conflict. For more information on resolving merge conflicts, see [Resolving merge conflicts](helpful-information/resolving-merge-conflicts).

Once everything is finished, you'll want to push it up to your branch on gitlab. Because you've modified the git history, you'll need to use the `--force` flag.

```shell
git push --force
```
Be sure not to `--force` push unless you really mean it... there is no going back. If you are working on a branch at the same time as someone else, use `--force-with-lease` to double check no one else has changed the git history since you did a `fetch`.

## Rebasing Prior to a Merge Request

Always squash your commits (see above) before rebasing your branch on another branch. It makes the process easier since you will only need to move one commit ahead of the rest of the commit tree.

First, update your refs to be sure you have the latest code:

```shell
 git fetch --all
```

Then, you want to checkout the Develop branch and use git pull to actually update to the latest code:

```shell
git checkout develop
git pull
``` 

Next is the actual rebase.

```shell
git checkout <your branch>
git rebase -i develop
```

This says to rebase your work onto `develop`.

From here the process looks similar to the squashing instructions above, though hopefully you followed our advice and you only have 1 commit in the file that appeared.

## Caveats
*Never*, ever, squash commits that are a part of the git history of `develop` or `master` unless a team decision has been reached. Once a commit has been put into develop, don't squash it, because that will make everyone's lives miserable. Only you can prevent git fires.

![Smokey Bear](https://media.giphy.com/media/11WjOaaVEE8eCk/giphy.gif)
