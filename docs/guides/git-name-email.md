# Setting your Git Name and Email

Before you push to Gitlab for the first time, you'll want to set up your git config `name` and `email`. Otherwise, Gitlab will freak out and deny your commits.

![](https://media.giphy.com/media/redG2SZ9O7f7a/giphy.gif)

To set this up, do the following:

```bash
$ git config --global user.name "Taylor, Riley Hal"
$ git config --global user.email "rileytaylor@email.arizona.edu"
```

Use your information, of course. Note that the name will match the one that AD generates as a full name. You can double check what both your name and email will be [here](https://gitlab.fso.arizona.edu/profile/).

## But I already made a commit and it still uses the computer email thingy!

If you've already made a commit before changing your config, you'll have to amend it:

```bash
$ git commit --amend --author="Author Name <email@address.com>"
```
