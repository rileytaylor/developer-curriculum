# Guides

This area of the site is for generally helpful information, such as the setup and usage of a npm or pip library, or how some of our internal tools work.

::: tip Have your own suggestions?
Great! Make a page! See [how you can contribute](/contributing).
:::
