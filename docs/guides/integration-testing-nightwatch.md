# Integration Testing with Nightwatch

Projects with integration testing enabled use [Nightwatch.js](http://nightwatchjs.org) to verify workflows. To run tests, you need the requisite browsers installed. Currently, tests are only run in Chrome by default because it is the most well-supported (as of mid-2018). However, if other browsers are added you must have them installed locally. If you run into issues with tests, be sure to update your browsers to the version recommended by the requisite driver (which uses Selenium's WebDriver API to interact with the browser to run the tests), or at a minimum update it to the latest version.

::: warning
Integration testing is funky. Just because tests aren't passing doesn't mean you are doing something wrong, or because the pass locally but not in CI. This is why we do not run them with the other tests, they are more of a... guideline... than a rule to live by. That doesn't diminish their importance in helping us diagnose issues.
:::

![gif](https://thumbs.gfycat.com/WholeEnchantingIndochinahogdeer-size_restricted.gif)

Integration testing is different from unit testing! Remember that integration testing is all about testing workflow. This is where cross-component functionality is tested, as opposed to unit testing where a components internals are tested. Essentially, all common user-tasks should have an integration test.

## Running tests

Running integration tests is different from running unit tests, mostly in that you also need to be running the application.

1. Run the django application in a vagrant box:
```shell
vagrant up && vagrant ssh # if you haven't already
rs
```

1. Build the frontend at least once:
```shell
# in another shell instance
npm install
npm run dev
```

1. Now, close out of the dev watcher (`ctrl + C`) and run the integration tests
```shell
npm run e2e
```

You should notice activity in the django server shell session and the testing shell session. If all goes well, all tests will succeed!

## Writing tests

For writing tests, see the [Nightwatch Developer Guide](http://nightwatchjs.org/guide). Bear in mind that there will be some subtle differences between Seleniums WebDriver API and the W3C WebDriver API (of which we use the latter since we are not running selenium).

To learn more about nightwatch in general, consider reading through [this guide on github](https://github.com/dwyl/learn-nightwatch)

