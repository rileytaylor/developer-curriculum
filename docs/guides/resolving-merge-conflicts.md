# Resolving Merge Conflicts

Welp, you gone done it. Way to go. Merge conflict. Well, looks like that's it. Pack your bags, get out of here.

![Merge](https://media.giphy.com/media/cFkiFMDg3iFoI/giphy.gif)

Merge conflicts happen all the time. Sometimes the problem is git, in which it thinks that you really need to make sure that the two blank lines instead of the one blank line are exactly what you want. Thanks, git. Other times it's because you've drastically changed a file and Git can't figure out the madness of your new order. Other times someone else on the team made an oopsie, or they edited the same file with a function that is eerily similar to yours. (This happens all the time in django settings files. Git seems to think environment variables are all the same, despite different names)

When this happens, you will probably see something like this:

```shell
error: could not apply fa39187... something to add to patch A

When you have resolved this problem, run "git rebase --continue".
If you prefer to skip this patch, run "git rebase --skip" instead.
To check out the original branch and stop rebasing, run "git rebase --abort".
Could not apply fa39187f3c3dfd2ab5faa38ac01cf3de7ce2e841... Change fake file
```

You have three options:
- You can run `git rebase --abort` to completely undo the rebase. Git will return you to your branch's state as it was before git rebase was called. Do this if you aren't sure that this conflict should arise, or if you want to go back and check something first.
- You can run `git rebase --skip` to completely skip the commit. Yeah, don't do this. Ever.
- You can fix the conflict.

### Fixing a merge conflict

Your first question is probably: "Wait, which files have problems?" That's a great question. 

![Wait... what?](https://media.giphy.com/media/j2nATOAdRgYZq/giphy.gif)

The easiest way to find out is via:

```shell
git diff --name-status
```

This command tells you which files a have conflicts. There are several letter codes to pay attention to: Added (A), Copied (C), Deleted (D), Modified (M), Renamed (R), have their type changed (T), are Unmerged (U), are Unknown (X), or have had their pairing Broken (B). Most of the time a merge conflict will show you files with the codes M and U, with U being the files you'll have to take a look at (mostly). Be sure to double check all files that show up in this list.

Go to each file and deal with the diffs. A diff will look like this:

```text
If you have questions, please
<<<<<<< HEAD
open an issue
=======
ask your question in IRC.
>>>>>>> new thing
```

`HEAD` represents the current tip of the branch, while `new thing` will be the commit that is waiting to be resolved. You'll need to manually edit this. Make sure you remove the conflict markers, too (`<<<<<<< HEAD`, `=======`, and `>>>>>>> new thing`). You really don't want to commit those.

Once you've done this in all the files, or verified that the status shown is what you want, you can now continue. You'll need to add all your changes to the commit and then continue the rebase.

```shell
git add .
git rebase --continue
```

That's it! You may run into a few more of these during a rebase, so be careful. If you think you are running into a lot of problems, you may want to make sure you have squashed all of your commits prior to making the rebase, or that you are doing things properly in the first place. Rebases are easy, but their effects can make life difficult for everyone else on the project. Be sure to always run tests and boot up the application after you perform a rebase to make sure things look fine, and don't force push the new git history up until you've made sure thigns are good to go.
