# Celery Configuration

Celery is an asynchronous task queue which allows us to schedule tasks or run functions on triggers. The uses are many, but include scheduled api calls to sources such as Active Directory and EPM, sending emails, slack notifications, and more.

![gif](https://media.giphy.com/media/Nx85vtTY70T3W/giphy.gif)

Configuring celery is actually quite easy, especially with the 4.2 update. Following this guide, you'll install and configure [Celery](http://docs.celeryproject.org/en/latest/index.html), [Redis](https://redis.io), and [Circus](https://circus.readthedocs.io/en/latest/), which all work together to make asyncronous tasks happen in our apps. Note that some of our apps use older configurations, and until they are updated some of this config may look different.

## Initial Configuration

First, add the following to `requirements/base.txt`:

```txt
django_redis==4.10.0
circus==0.15.0
celery==4.2.0
```
There may be newer versions of software, feel free to update to them after consulting the changelog. If there are changes, please edit this document.

Second, you'll want to ensure Redis is installed locally and on the server. Somewhere in `setup/scripts/local.bash` and `setup/scripts/production.bash` add the following. Best to put it around line 25 in both files where other apt packages are being installed. You'll want to manually run this command inside your vagrant shell as well to have it, or you could just reprovision the vagrant box instead.

```shell
# redis
apt-get install -y redis-server
```

Next, create a folder called `taskapp` where you create other django apps, as shown below:

```
appname
|- config
|- appname
|  |- polls
|  |- taskname
|     |- __init__.py
|     |- celery.py
|- __init__.py
```

Inside `taskapp/celery.py`, paste the following and replace `[appname]` with the applications actual name:

```py
from __future__ import absolute_import

import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.local')

app = Celery('[appname]')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request)) # pragma: no cover
```

Then, in the `__init__.py` at the same level as the new `taskapp` folder, add the following:

```py
from __future__ import absolute_import, unicode_literals

# This will make sure the app is always imported when django starts
# so that the shared_task will use this app.
from .taskapp.celery import app as task_app

__all__ = ['task_app']
```

Now, edit the base settings at `config/settings/base.py`. Add the following below the `# Your common stuff:...` comment:

```py
# Celery Config
CELERY_BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = TIME_ZONE
```

Additionally, edit the production settings at `config/settings/production.py`. Add the following below the `# Your common stuff:...` comment:

```py
# CACHING
# ------------------------------------------------------------------------------
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}
```
This will ensure that `django-redis` is set as the cahching engine and that redis is running on the production server.

That's it! To test, run the django server (`rs`) to make sure there aren't any syntax problems or missing pieces. Next, open 2 vagrant sessions with `vagrant ssh`. In the first, run `redis-server`, which starts redis. Celery uses redis to schedule its tasks and output logging. In the second, run `celery -A <appname> worker -l info` in the same directory as your `manage.py` file. This will start celery. As long as it boots up nicely and indicates it has loaded the `debug_task`, then we are good to go!

If you want to be 100% sure everything is running, open another vagrant session with `vagrant ssh` and then enter the shell with `python manage.py shell_plus`. Do the following:

```py
>>> from <appname>.taskapp.celery import debug_task
>>> debug_task.delay()
```

`.delay()` executes a celery task immediately. If there are arguments or kwargs, you can pass those inside the delay() call. Once you execute, you should see the shell return an AsyncResult object. Additionally, you should see activity in the terminal session where the celery worker is running, similar to the following:

![image](/uploads/celery-console.png)

If you have issues, or for more information, see [Celery's documentation for using it with Django](http://docs.celeryproject.org/en/latest/django/first-steps-with-django.html). There is also an example repository [here](https://github.com/celery/celery/tree/master/examples/django/).

For more information on calling tasks, see [Tasks documentation](http://docs.celeryproject.org/en/latest/userguide/calling.html).

## Creating Tasks for Apps
In this configuration, you shouldn't touch the `taskapp` folder anymore now that the initial configuration is set up. In every app that will need to use celery, create a `tasks.py` file (it will live right next to `views.py`, `serializers.py`, etc.)

It should look similar to the following:

```py
"""
Celery Tasks for the some-app-name app

Functions
---------
sample_task: just a sample task 

"""
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)

@app.task
def sample_task():
    """Add 2 + 2"""
    logger.info('Adding...')
    return 2 + 2
```

`logger` allows you to log to the celery logger, which is just a wrapper of the python logger. Be sure to use this so that debugging is made easier.
