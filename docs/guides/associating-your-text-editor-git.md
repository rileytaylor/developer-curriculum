# Associating your Text Editor with Git

By default git will use the default text editor of your terminal... usually `vi` on macOS, and possibly `nano` on some linux distro's. If you like using it the way it is don't feel obligated to change it, but it's generally an easier experience to edit git prompts with your text editor. Multicursor when squashing 20 git commits is probably the biggest use case. The good thing is most editors support this capability and routinely improve the experience.

The following terminal command will get it done for your text editor of choice:
- **Atom:** `git config --global core.editor "atom --wait"`
- **Visual Studio Code:** `git config --global core.editor "code --wait"`
- **Sublime Text:** `git config --global core.editor "subl -n -w"`
- **Vim:** `git config --global core.editor "vim"`
- Other command line-based editors can be added the same way `vim` is added: that includes `nano`, `emacs`, and more.

That covers the big ones. Is your editor not listed? Stack Overflow will likely have the answer. [Here is a thread to get you started](http://stackoverflow.com/questions/2596805/how-do-i-make-git-use-the-editor-of-my-choice-for-commits)

Some editors require some sort of `--wait` operator. Without it, git may try and proceed before you've made any edits to the configuration file. If you are seeing weird behavior with your editor while attempting to rebase, that may be the problem.
