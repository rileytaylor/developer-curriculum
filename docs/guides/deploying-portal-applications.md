# Deploying Portal Applications

Most portal applications are configured with a CI/CD pipeline, meaning that deploys should be automatic when files are committed to the `develop` and `master` branches of a project. (`develop` routes to the testing server, while `master` routes to the production server.

Once that's done, a full-time developer (i.e. someone with server access) can ssh onto the server and run a few commands to ensure things are up to date. Once on the server, the following commands will finalize the deployment:
```shell
cd /var/www/<appname>/<appname>    # go to the app directory
workon <appname>                   # activate the virtualenv
. /etc/apache2/envvars             # source the environment variables
python manage.py collectstatic -c  # collect staticfiles
sudo service apache2 restart       # restart apache
```

Then the app should be redeployed within a few seconds.

Some apps will need a few more steps, such as installing from bower or npm, but sooner or later all apps will be good to go with the above methods. If you have questions about deploying an app, ask!

## First-Time Setup
If the server is being setup for the first time (or if it's been wiped off of the face of the earth for the beginning of a new world order), you'll need to do a few things first. These instructions do not include the setup of enviornment variables, shibboleth, or ssl. See the project's README.md for imnformation on this. Some of the steps listed there are replicated here. Use the information here over what is in the README.

1. Create the project folder, or make sure it exists: `mkdir /var/www/<appname>` and ensure it's set for the `developer` group with: `sudo chown www-data:developer /var/www/<appname>`
1. Run the deploy pipeline in gitlab for the `develop` or `master` branch, depending on the server. Once it succeeds, you should find your `/var/www/<appname>` folder is now populated by a fresh group of evolved files, ready to rebuild the earth and then ruin it again through global warming and nuclear warfare.
1. We need to win a swift war to control the newly evolved files and establish the Xth Reich. `sudo chown scriptuser:scriptuser /var/www/<appname>` 
1. Run the provisioning script: `sudo setup/scripts/production.bash`. If this fails, refer to step 3. Now the ~~people~~ files are working for us and have built the greatest ~~military~~ .virtualenv that will bring on a thousand years of prosperity.
1. give the power back to the ~~führer~~ CD server so that it can deploy things: `sudo chown www-data:developer /var/www/<appname>` If this app uses npm, also run `sudo chown cd:developer /var/www/<appname>/<appname>/node_modules` (make sure that's the correct path first!)
1. Run another deploy from gitlab to ensure things are copying over properly now. If you have problems, refer to step 5.
1. Take command of the ~~SS~~ .virtualenv: `cd /var/www/<appname>/<appname>` and `workon <appname>`
1. Import env variables: `source /etc/apache2/envvars`
1. If the app uses gatekeeper, install it (most apps do): `pip3 install --index-url https://zealot.fso.arizona.edu/repository/pypi/simple gatekeeper`
1. Install uashib: `pip3 install --index-url https://zealot.fso.arizona.edu/repository/pypi/simple uashib`
1. Make migrations. If the database already exists, these migrations will be made but not applied. `python manage.py makemigrations` and `python manage.py migrate`. If you don't see the apps included, you'll have to manually add them to the list (i.e. `python manage.py makemigrations app1 app2`)
1. Collect staticfiles: `python manage.py collectstatic -c`
1. DEPLOY: `sudo service apache2 restart` (`start` if this hasn't been done before)

Note that some applications will have more steps, such as running circus.

Some apps don't build js assets in CI (basically any app that isn't vue.js doesn't do anything in CI for the frontend) for these apps, you'll need to manually `npm install` in /var/www/appname.

You can check permissions at any time with `ls -lah`.

## CD is failing due to file permissions
This happens whenever someone hops on and executes things like `npm install` or `pip install` manually. Often, this is a result of necessary debugging. To resolve this issue, you'll need to modify the user and group permssions. Only administrators (I.e. Senior developers and sysadmins) can resolve these issues:

First, make sure the root of the project and all files down have `www-data:developer` permissions:
```shell
cd /var/www/<appname>
sudo chown -R www-data:developer .
```
Then, ensure the `node_modules` folder has `cd:developer` permissions:
```shell
cd /var/www/<appname>/<appname>/node_modules
sudo chown -R cd:developer .
```
At this point, deploys should work. `npm` installs files within packages as 644, which prevents it from copying node modules to the server if it is not the user of those files. Otherwise, everything else in the virtualenv and the actual code should work as long as the group is set to `developer`.

## App won't run because Gatekeeper and/or uashib aren't installed
This happens if the virtualenv needs to be nuked and rebuilt (i.e. `rmvirtualenv <appname>` then `bash setup/scripts/production.bash`), which needs to happen from time to time. Simply reinstall the apps with the following command:
```shell
pip3 install --index-url https://zealot.fso.arizona.edu/repository/pypi/simple gatekeeper uashib
```
*Note:* Be sure you are in the app's activated virtualenv before installing like this, otherwise you'll install the packages to the server and that won't help you at all.
