# Portal Application FAQ

![portal gif](https://media.giphy.com/media/8V4VaJJzOS5S8/giphy.gif)

## What is a portal application?

A portal application is an application that is built by FSO using `django-base-app`... usually. "Portal" is more of a design paradigm in which apps are decoupled but use the same internal components, which are usually kept in sync with `django-base-app`. All portal apps are listed at https://portal.fso.arizona.edu. In the future there could be portal apps that have nothing to do with django, so the best definition of a "Portal Application" is that it is an app developed by FSO and isn't one of the previous highly-coupled ISW or Titan app platforms.

## User administration

Portal apps use [Gatekeeper](https://gitlab.fso.arizona.edu/fast/gatekeeper) to manage user access. Gatekeeper is essentially a middleware that communicated with Active Directory to keep track of who has access or not. Have someone go to the application and click on the `Request Access` button that appears. Then, they can go ahead and be approved by someone who has user admin powers (generally anyone else who has access to the application).

There will also be an Active Directory user group that stores the data. When apps are created or if a lot of users need to be added they can also be added to the user group through AD. The user groups for each portal app live at `FSO.Arizona.EDU/Groups/Portal` inside of AD.

## Going to `/admin` says that I don't have staff privileges!!!

Being a user admin is different from actually having staff privileges. To access Django Admin, you need staff privileges, which is managed in the app itself as opposed to coming through Gatekeeper from AD. If you get this you have two options: 

1. Ask someone else who already has staff privileges to add you to the `staff_user` list in Django Admin.
1. Simply ssh into the requisite server and run the following commands:

```shell
cd /var/www/<appname>/<appname>
. /etc/apache2/envvars
workon <appname>
python manage.py make_me_admin --u <username>
```

*Note:* your username will be your netid@arizona.edu
