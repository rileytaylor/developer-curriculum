# Vagrant FAQ

To save some time (and battery), if you aren't using your vagrant box, run `vagrant suspend`. Then, when you need it, `vagrant resume && vagrant ssh`.

If your box freezes up, try using a `vagrant reload`. If that doesn't solve things, then you'll need to `vagrant destroy -f` and re-create and re-provision the box.

## I lost the terminal with the server I was running...

If you had a server running, then accidentally closed the window... never fear! You probably tried restarting the server, but found it errored out since the port was in use. You need to manually kill the process.

In the vagrant box, type:

```shell
lsof -i :8000
```

(If you aren't using Django, sub the port number `:xxxx` with whatever the port is that is used)

This should return a table with the server process. Find the `PID` (in this next example the PID was 37284, make sure you use the returned PID), and then run:

```shell
sudo kill -9 37284
```

Now you should be able to restart the web server you were unable to start previously.

# Running two vagrant machines on your computer

To run two machines on your computer, you'll need to modify the config file.

**Note:** If you make any changes to the Vagrantfile, make sure you *undo* those changes before you merge!

Pick one of your boxes. Find the line with:

```shell
config.vm.network "forwarded_port", guest: 8000, host: 8000
```

Change it to:

```shell
config.vm.network "forwarded_port", guest: 8000, host: 8001
```

If the modified box is currently running, you'll need to use `vagrant reload` to update its configuration.

# Publicly sharing your vagrant box 

If you need to test your application outside of your local machine, for IE/Edge/mobile testing as an example, Vagrant has a `vagrant share` command which generates a sharable temporary URL. This requires [ngrok](https://ngrok.com/) installed on your machine which can be found directly from their website, but it is recommended to install `ngrok` through homebrew.

Once ngrok is installed you will also need the `vagrant-share` plugin which can be installed by running `vagrant plugin install vagrant-share` if it's not already on your computer. Once ngrox and the vagrant-share plugin are installed ssh in to the vagrant box and start the runserver, then in another terminal window in the same project directory run `vagrant share` to generate a public facing temporary URL.
