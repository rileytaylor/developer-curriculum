---
sidebar: auto
---

# Contributing

Contributing to this docs site is easy. Everything is in [Markdown](https://daringfireball.net/projects/markdown/) and you probably already know what that is. If not, welcome to the greatest formatting language in the world, and you'll love it so much you will probably start writing everything in markdown. It's just fantastic.

Getting off of the soapbox.

## Adding information
Simply treat this like any other project! However, for ease-of-use on quick changes, consider using Gitlab's WebIDE to make changes. If you use the WebIDE, be sure to check out the test deployment of your site before merging!

1. Find the page you want to edit, or create a new one (for new information) in the appropriate area. Keep your filename simple but descriptive enough to differentiate it from other pages. `faq` is a bad name, but `thing-faq` is better.
   - `/docs/curriculum/` is for learning resources for new developers
   - `/docs/guides/` is for helpful tidbits or guides for technologies we use often or rarely in our applications
   - `/docs/what-i-use/` is for individual user vanity pages to display all the cool things you use and which you think everyone else should use
2. Each page, at a minimum, should have a h1 header (`#`), which will title the page in the sidebar. Anything that uses an h2 (`##`) will appear in the sidebar once the page is selected. Otherwise, formatting is up to you. See sections further down on this page for some specific formatting options available to you.
3. Add your page to the sidebar config. Use `docs/.vuepress/config.js` and add your page to the appropriate spot:
```js
...
themeConfig: {
  ...
  sidebar: {
    ...`somewhere in here`...
  }
},
...
```
You'll notice that each main area has a seperate sidebar, and many of those areas have an object grouping pages together neatly. For more information on sidebar config, see the [Vuepress docs](https://v1.vuepress.vuejs.org/theme/default-theme-config.html#sidebar).
4. Make a commit in a seperate branch and create a merge request. You cannot directly commit to `master`, and this is less for protection of the branch and more to ensure we don't just commit things without thinking. Once you have a merge request, review your changes before merging. You can find the link to your test site under the Operations -> Enviornments tab in Gitlab, and also on the merge request page, which you can use to verify that your changes are good. We do not have merge request approvals on in this project (on purpose), but you can have someone review your changes if you'd like, particularly if this is your first time contributing!
5. This repo uses the Merge Commit strategy (on purpose), so you can resolve conflicts right in the merge request without needing to rebase. Some things may be more complicated, however, and require some manual intervention. Be sure to resolve any conflicts before merging.
6. Once the merge request is merged, the main site will be updated. All Done!

## Markdown Features
In addition to standard markdown, there are several additional features available for use.

::: tip
Most (but not all) of the following is a summary of information available in the VuePress docs. If something isn't working or you want more detailed information, check [this page out](https://v1.vuepress.vuejs.org/guide/markdown.html#header-anchors). Please update any of this information if something in VuePress has changed and we haven't caught it yet!
:::

### Custom Blocks
Custom blocks are enabled in this app, and several are built in. You've seen these used throughout the docs area:

::: tip This is a tip block
Use this for important callouts and suggestions.
:::

```md
::: tip This is a tip block
Use this for important callouts and suggestions.
:::
```

::: warning Warning!
Use this for important, possibly destructive or difficult processes.
:::

```md
::: warning Warning!
Use this for important, possibly destructive or difficult processes.
:::
```

::: danger Danger Zone!
Use this for incredibly dangerous activities
:::

```md
::: danger Danger Zone!
Use this for incredibly dangerous activities
:::
```

### Images and Gifs
Images can be added using the normal markdown syntax:
```md
![placeholder text](image-link)
```

If you need to attach an image that doesn't exist elsewhere on the internet, you'll need to add it to this project. Images can be placed in the `docs/.vuepress/public/uploads` folder. Please try to keep them as small as possible. Once there, you can reference them like so:
```md
![placeholder text](/uploads/image-name.extension)
```

Gifs are added just like images. If using services like [Giphy](https://giphy.com), try to use full links instead of shortened ones.

![gif](https://media.giphy.com/media/SxQDc2jZchCgM/giphy.gif)

### Videos
Videos in markdown are a bit more complicated, especially since videos don't work as smoothly in html as images do.

This site is configured with [markdown-it-html5-embed](https://github.com/cmrd-senya/markdown-it-html5-embed), which will allow for video links to be embeded as `<video></video>` elements. Neat!

```md
![](https://assets.cdn.fso.arizona.edu/it_crowd_fire.mp4)
```

![](https://assets.cdn.fso.arizona.edu/it_crowd_fire.mp4)

This works for videos inside of this repo too! Like images, place the video in `docs/.vuepress/public/uploads` and link to it the same way. As with images (but especially for videos!) keep these small! Use compression!

#### Youtube, Vimeo

For these services, you can utilize `markdown-it-video` (which is installed in this project) to render a nice inline video:

```md
@[youtube](video-id-goes-here)
```

@[youtube](p1iLqZnZPdo)

```md
@[vimeo](321153440)
```

@[vimeo](321153440)

For more information, see [@centerforopenscience/markdown-it-video's documentation](https://github.com/CenterForOpenScience/markdown-it-video)

#### The Hijack method
If you can't use the above method, you'll need to hijack the image syntax a bit. For example, try the following for youtube:

The easiest way to embed youtube videos is a hijack of the image tags, using Youtube's api to put a placeholder image in the way of the video link.

```md
[![IMAGE ALT TEXT](http://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg)](http://www.youtube.com/watch?v=YOUTUBE_VIDEO_ID_HERE "Video Title")
```
the `0.jpg` corresponds to a "frame", so you can always find a better frame than the first the video displays.

So, for example that might look like this:

[![Sample Video Embed](http://img.youtube.com/vi/p1iLqZnZPdo/0.jpg)](http://www.youtube.com/watch?v=dQw4w9WgXcQ "Sample Video Embed")

You can click on the placeholder in order to watch it.

Essentially, if you can use a placeholder image and link to a video elsewhere the same way, you can use this method.

::: tip What about iframe embeds?
If the service you are linking a video from supports embedding via an `iframe` or similar, try using that and see what happens. 
:::

### Emoji :tada:
Channel your inner millenial and add emoji to pages.

:tada: :100:

```md
:tada: :100:
```

You can find all the [usable emoji here](https://github.com/markdown-it/markdown-it-emoji/blob/master/lib/data/full.json).

### Tables
You can use "Github-style" tables.

```md
| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |
```

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

### Table of Contents
If you have a particularly long page, you might consider adding a table of contents to the top of it. You can do that like so:
```md
[[toc]]
```

And it will look like this:

[[toc]]

### Syntax Highlighting
To set up syntax highlighting, use three backtics followed by a language specifier (if there is one), and close it with another 3 backticks. We often use `js`, `python`, `html`, `css`, `md`, and `shell`. For more information, see the [VuePress docs](https://v1.vuepress.vuejs.org/guide/markdown.html#syntax-highlighting-in-code-blocks).

You can also highlight a specific line using `{X}` after the language specifier.

<pre><code>
``` js {4}
export default {
  data () {
    return {
      msg: 'Highlighted!'
    }
  }
}
```
</code></pre>

``` js {4}
export default {
  data () {
    return {
      msg: 'Highlighted!'
    }
  }
}
```

If you are having issues, you can also use an html `<code></code>` or `<pre></pre>` element to wrap some code, it just won't be as nice looking.

### HTML in Markdown
You can use any valid html element in markdown! HTML away!

### CSS/JS in markdown
You can place blocks of css and javascript inside of a markdown file. This could allow for some interesting documentation, but do try to avoid doing this if at all possible. To do so, simply write a `<script></script>` or `<style></style>` block and go to town. For more information, see [the VuePress docs](https://v1.vuepress.vuejs.org/guide/using-vue.html#script-style-hoisting).

### Vue in Markdown
Since this site is compiled with vue, you can also use Vue components and features. For more information on this, see [the VuePress docs](https://v1.vuepress.vuejs.org/guide/using-vue.html#using-vue-in-markdown).

That means you can use handlebars syntax:

```md
2 + 2 = {{ 2 + 2 }}
```

2 + 2 = {{ 2 + 2 }}

That also means you can [use vue directives](https://v1.vuepress.vuejs.org/guide/using-vue.html#directives) and [render variables stored in the YAML frontmatter](https://v1.vuepress.vuejs.org/guide/using-vue.html#access-to-site-page-data) of an individual page.

#### Built-in Components
There are a few built-in components that you can make use of, which are described [here](https://v1.vuepress.vuejs.org/guide/using-vue.html#built-in-components).

#### Writing Vue Documentation
With that in mind, sometimes you want to escape your vue so that you can write an example. To do so, use a custom block:

```md
::: v-pre
`{{ this will be displayed as-is rather than compiled from a variable }}`
:::
```

::: v-pre
`{{ this will be displayed as-is rather than compiled from a variable }}`
:::

## Frontmatter
You can utilize YAML Frontmatter to store variables and set individual page data utilized by VuePress to render the page. This is great if you want to use some sort of generative rendering to aid your documentation. See [the VuePress docs](https://v1.vuepress.vuejs.org/guide/frontmatter.html#front-matter).
